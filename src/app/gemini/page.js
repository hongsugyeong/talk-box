'use client'

import { useState } from "react";
import axios from 'axios';

export default function Page() {
    const [question, setQuestion] = useState('')
    const [response, setResponse] = useState('')

    const handleQuestion = e => {
        setQuestion(e.target.value)
    }

    const handleSubmit = e => {
        e.preventDefault()

        // 주소 받아오기
        const apiUrl = 'https://generativelanguage.googleapis.com/v1/models/gemini-1.5-flash:generateContent?key=API_KEY'

        // body 준비
        const data = {"contents":[{ "parts":[{"text": question}]}]}

        // axios를 사용해 post 하기, apiUrl 연결 후 data 받기
        axios.post(apiUrl, data)
            .then(res => {
                setResponse(res.data.candidates[0].content.parts[0].text)
                //  data => candidates[0] => content => parts[0] => text
                // 포스트맨 보면서 데이터 가져오기
            })
            .catch(err => {
                console.log(err)
            })

    }

    return (
        <main className="mth-main-background min-h-screen">
            <div className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 flex flex-col">
                <form onSubmit={handleSubmit}>
                    <input type="text" value={question} onChange={handleQuestion} placeholder="무엇이 궁금한가요?"/>
                    <button type="submit">확인</button>
                    {/* submit 처리를 하게 되면 마우스 클릭 안 해도 엔터로 처리 가넝 */}
                </form>
                {response && <p>{response}</p>}
                {/* response가 있을 경우 response 보여주기, &&으로 and 연산자 이용한 거임 */}
            </div>
        </main>
    )
}