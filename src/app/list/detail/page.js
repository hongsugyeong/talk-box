export default function Page() {
    return (
        <div className="mth-main-background min-h-screen">
            <div className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2">
                <h1 className="max-w-2xl text-8xl font-semibold">디테일이지롱</h1>
            </div>
        </div>
    )
}
