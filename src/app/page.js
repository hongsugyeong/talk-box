export default function Home() {
    return (
        <main className="mth-main-background min-h-screen">
            <div className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 flex flex-col">
                <h1 className="max-w-2xl text-8xl font-semibold">너에게 전한다</h1>
                <button
                    className="before:ease top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 relative h-12 w-40 mt-10 overflow-hidden border border-blue-500 text-blue-500 shadow-2xl transition-all before:absolute before:top-1/2 before:h-0 before:w-64 before:origin-center before:-translate-x-20 before:rotate-45 before:bg-blue-500 before:duration-300 hover:text-white hover:shadow-blue-500 hover:before:h-64 hover:before:-translate-y-32">
                    <span className="relative z-10 text-3xl font-semibold">사진 첨부</span>
                </button>
            </div>
        </main>
    );
}
